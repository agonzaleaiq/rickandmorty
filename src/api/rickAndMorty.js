import axios from "axios";

const instance = axios.create({
    baseURL: 'http://localhost:8080',
    headers: {
        Authorization: `Bearer ${localStorage.getItem('access-token')}`
    }
})
const login = async (payload) => {

    const res = await instance.post('/user/login', payload)
    if (res.status === 200) {
        saveLocalStorage(res)
    }
    return {message: res.data.message, status: res.status}
}

const register = async (payload) => {
    const response = await instance.post('/user/register', payload)
    if (response.status === 201) {
        saveLocalStorage(response)
    }
    return {message: response.data.message, status: response.status}
}

const characters = async (payload) => {
    const response = await instance.get('/characters', {params: payload});
    return {characters: response.data.result, info: response.data.info}
}

const favorites = async () => {
    const response = await instance.get('/favorites', {
        params: {
            id_user: localStorage.getItem('id')
        }
    })
    return {result: response.data.result}
}

const addFavorite = async (payload) => {
    await instance.post('/favorites', payload)
}

const removeFavorite = async (payload) => {
    await instance.delete('/favorites/' + payload.idCard, {
        data: payload
    })
}

const saveLocalStorage = (res) => {
    localStorage.setItem('name', res.data.result[0].name)
    localStorage.setItem('email', res.data.result[0].email)
    localStorage.setItem('id', res.data.result[0].id)
    localStorage.setItem('access-token', res.data.token)

    window.history.pushState({}, '', '/characters')
    const navEvent = new PopStateEvent('popstate')
    window.dispatchEvent(navEvent)
}
// eslint-disable-next-line import/no-anonymous-default-export
export default {
    login,
    register,
    characters,
    favorites,
    addFavorite,
    removeFavorite
}