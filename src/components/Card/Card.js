import React from 'react'
import './Card.scss'
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';


const Card = props => {

    const favouriteButton = props.isFavorite
        ? <FavoriteIcon className='card__fav' onClick={(event) => props.onClickFavButton(event, props
            .character.id, props.isFavorite)}/>
        : <FavoriteBorderIcon className='card__fav' onClick={(event) => props.onClickFavButton(event, props
            .character.id, props.isFavorite)}/>

    return (
        <div className='card'>
            <img src={props.character.image} alt='avatar'/>
            <div className='card__content'>
                <h2>{props.character.name}</h2>
                <span className='card__status'><FiberManualRecordIcon
                    className={'card__icon card__icon__' + props.character.status}/>{props.character.status} - {props.character.species}</span>
                <span className='text__grey'>Last know location:</span>
                <span>{props.character.location.name}</span>
                <span className='text__grey'>First seen in:</span>
                <span>{props.character.origin.name}</span>
            </div>
            {favouriteButton}
        </div>
    )
}

export default Card;