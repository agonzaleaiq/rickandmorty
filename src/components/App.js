import React, {useEffect, useState} from 'react'
import Header from "./Header/Header";
import Content from "./Content/Content";
import rickAndMorty from "../api/rickAndMorty";
import {List} from "@material-ui/icons";
import FavoriteIcon from "@material-ui/icons/Favorite";
import Route from "./Route";
import Register from "./Access/Register";
import Login from "./Access/Login";
import '../style/Unregister.scss'
import Paginate from "./Paginate/Paginate";

const App = () => {

    const [characters, setCharacters] = useState([])
    const [info, setInfo] = useState([])
    const [name, setName] = useState('')
    const [favorites, setFavorites] = useState([])


    useEffect(() => {
        rickAndMorty.characters().then(response => {
            setCharacters(response.characters)
            setInfo(response.info)
            setName(localStorage.getItem('name'))
        })

        if (localStorage.getItem('id')) {
            getFavourites()
        }
    }, [name])


    const saveUser = () => {
        setName(localStorage.getItem('name'))
        getFavourites()
    }

    const getFavourites = () => {
        rickAndMorty.favorites().then(response => {
            setFavorites(response.result)
        })
    }

    const logout = () => {
        localStorage.removeItem('name')
        localStorage.removeItem('id')
        localStorage.removeItem('email')
        setName('')
        setFavorites([])
    }

    const onClickFavButton = async (id, isFavorite) => {
        if (localStorage.getItem('id')) {
            const payload = {
                idUser: parseInt(localStorage.getItem('id')),
                idCard: id,
            }
            if (!isFavorite) {
                await rickAndMorty.addFavorite(payload)
            } else if (localStorage.getItem('id') && isFavorite) {
                await rickAndMorty.removeFavorite(payload)
            }
            getFavourites()
        } else {
            goRegisterPage()
        }
    }

    const goRegisterPage = () => {
        window.history.pushState({}, '', '/register')

        const navEvent = new PopStateEvent('popstate')
        window.dispatchEvent(navEvent)
    }

    const goPage = async (number) => {
        const payload = {
            page: number
        }
        rickAndMorty.characters(payload).then(response => {
            setCharacters(response.characters)
            setInfo(response.info)
            setName(localStorage.getItem('name'))
        })
    }

    const data = [
        {
            url: '/characters',
            name: 'Characters',
            icon: <List className="icon"/>
        },
        {
            url: '/favorites',
            name: 'Favorites',
            icon: <FavoriteIcon className="icon"/>
        }
    ]
    const paths = ['/characters', '/favorites', '/register', '/login', '/']

    const favPage = (
        localStorage.getItem('id') ? <div>
                <Content
                    characters={characters}
                    info={info}
                    favorites={favorites}
                    onClickFavButton={onClickFavButton}
                    goPage={goPage}
                    isFavPage={true}
                />
                <Paginate info={info} goPage={goPage}/>
            </div>
            : <div className='unregister'>
                <h1>Login for have favorites cards</h1>
                <span className='access__link' onClick={goRegisterPage}>Click hear</span>
            </div>
    )
    return (
        <div>
            <Header data={data} name={name} logout={logout}/>
            <Route path='/login'>
                <Login saveUser={saveUser}/>
            </Route>
            <Route path='/register'>
                <Register saveUser={saveUser}/>
            </Route>
            <Route path='/characters'>
                <Content
                    characters={characters}
                    info={info}
                    favorites={favorites}
                    onClickFavButton={onClickFavButton}
                    goPage={goPage}
                    isFavPage={false}
                />
                <Paginate info={info} goPage={goPage}/>
            </Route>
            <Route path='/favorites'>
                {favPage}
            </Route>
            {!paths.includes(window.location.pathname) ?
                <div className='unregister'>
                    <h1>404 Page not found</h1>
                    <span className='access__link' onClick={goRegisterPage}>Click hear</span>
                </div> :
                null}
        </div>
    )
}

export default App;