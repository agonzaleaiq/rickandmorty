import React from "react";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import './Paginate.scss'


const Paginate = (props) => {

    const nextPage = async (event) => {
        event.preventDefault();
        const split = props.info.next.split('=')
        props.goPage(split[1])
    }

    const prevPage = async (event) => {
        event.preventDefault();
        const split = props.info.prev.split('=')
        props.goPage(split[1])
    }

    const goPage = (e) => {
        props.goPage(e.target.innerText)
    }

    let isActivePrev = props.info.prev ? 'active' : 'inactive'
    let isActiveNext = props.info.next ? 'active' : 'inactive'

    let numbers = Array.from({length: props.info.pages}, (v, i) => {
        const page = props.info.next ? props.info.next.split('=')[1] - 1 : parseInt(props.info.prev.split('=')[1]) + 1
        return (
            <span
                className={i + 1 === page ? 'press__number' : ''}
                key={i + 1}
                onClick={(e) => goPage(e)}>
                    {i + 1}
            </span>
        )
    })

    return (
        <div className='paginate'>
            <ArrowBackIosIcon
                className={`paginate__${isActivePrev}`}
                onClick={(event) => prevPage(event)}/>
            <div className='paginate__numbers'>{numbers}</div>
            <ArrowForwardIosIcon
                className={`paginate__${isActiveNext}`}
                onClick={(event) => nextPage(event)}/>
        </div>
    )

}

export default Paginate