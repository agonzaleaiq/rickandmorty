import React from "react";
import './Content.scss';
import Card from "../Card/Card";


const Content = (props) => {

    const onClickFavButton = (event, id, isFavorite) => {
        event.preventDefault()
        props.onClickFavButton(id, isFavorite)
    }

    const card = props.characters.map(character => {
        return <Card key={character.id} character={character}
                     isFavorite={props.favorites.includes(character.id)}
                     onClickFavButton={(event) => onClickFavButton(event, character.id, props.favorites.includes(character.id))}/>
    })
    const favCard = props.characters.map(character => {
        if (props.favorites.includes(character.id)) {
            return <Card key={character.id} character={character} isFavorite={true}
                         onClickFavButton={(event) => onClickFavButton(event, character.id, true)}/>
        }
        return null
    })
    const renderedCards = props.isFavPage ? favCard : card;
    return (
        <div className='content'>
            {renderedCards}
        </div>
    )

}

export default Content;