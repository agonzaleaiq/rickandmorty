import React from "react";

const Input = (prop) => {
    return (
        <div className='access__cell'>
            <label className='field'> {prop.name}</label>
            <input
                type={prop.type}
                value={prop.value}
                onChange={(e) => prop.setValue(e.target.value)}/>
        </div>
    )
}

export default Input