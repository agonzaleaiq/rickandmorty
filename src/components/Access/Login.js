import React, {useState} from "react";
import './Access.scss'
import rickAndMorty from "../../api/rickAndMorty";
import Link from "../Link";
import Input from "./Input";


const Login = (props) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('')
    const [status, setStatus] = useState(false)


    const onFormSubmit = async (event) => {
        event.preventDefault();
        const payload = {
            email: email,
            password: password,
        }
        rickAndMorty.login(payload).then(r => {
            setMessage(r.message)
            setStatus(r.status !== 212)
            if (r.status !== 212)
                props.saveUser()
        })
    }
    const showMessage = () => {
        if (message !== '')
            return (<div
                className={`access__response access__response--${status ? 'correct' : 'declined'}`}>
                {message}
            </div>)
    }
    return (
        <div className='access'>
            <form className='access__form'>
                <Input type='text' name='Email' value={email} setValue={(value) => setEmail(value)}/>
                <Input type='password' name='Password' value={password} setValue={(value) => setPassword(value)}/>
                <button onClick={(event) => onFormSubmit(event)}>Submit</button>
                {showMessage()}
                <Link href="/register" className='access__link'>
                    <span>Do you dont have an account? Register</span>
                </Link>
            </form>
        </div>
    )
}

export default Login;