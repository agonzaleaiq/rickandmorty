import React, {useState} from "react";
import './Access.scss'
import rickAndMorty from "../../api/rickAndMorty";
import Link from "../Link";
import Input from "./Input";


const Register = (props) => {

    const [email, setEmail] = useState('')
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')
    const [message, setMessage] = useState('')
    const [status, setStatus] = useState(false)

    const onFormSubmit = async (event) => {
        event.preventDefault();
        const payload = {
            name: name,
            email: email,
            password: password,
        }

        rickAndMorty.register(payload).then(r => {
            setMessage(r.message)
            setStatus(r.status !== 212)
            if (r.status !== 212)
                props.saveUser()
        })
    }
    const showMessage = () => {
        if (message !== '')
            return (<div
                className={`access__response access__response--${status ? 'correct' : 'declined'}`}>
                {message}
            </div>)
    }
    return (
        <div className='access'>
            <form className='access__form'>
                <Input type='text' name='Name' value={name} setValue={(value) => setName(value)}/>
                <Input type='text' name='Email' value={email} setValue={(value) => setEmail(value)}/>
                <Input type='password' name='Password' value={password} setValue={(value) => setPassword(value)}/>
                <button onClick={(event) => onFormSubmit(event)}>Submit</button>
                {showMessage()}
                <Link href="/login" className='access__link'>
                    <span>Do you have account? Sing in</span>
                </Link>
            </form>
        </div>
    )
}

export default Register;