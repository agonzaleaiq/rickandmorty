import React, {useState} from "react";
import Link from "../Link";
import {AccountCircle} from "@material-ui/icons";

const AccessTab = (props) => {
    const [openMenu, setOpenMenu] = useState(false)

    const logout = () => {
        props.logout()
        setOpenMenu(!openMenu)
    }
    const menu = () => {
        if (openMenu) {
            return (
                <div className='header__menu' onClick={logout}>
                    <span>Logout</span>
                </div>
            )
        }
    }
    return (
        <div className="header__tabs header__access">
            <hr className="hr"/>
            {props.name ?
                <div>
                    <span onClick={() => setOpenMenu(!openMenu)}>{props.name}</span>
                    {menu()}
                </div>
                :
                <Link href="/login" children='login'>
                    <AccountCircle className="icon"/>
                    Login
                </Link>
            }
        </div>
    )
}

export default AccessTab