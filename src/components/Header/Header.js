import React from "react";
import Link from "../Link";
import './Header.scss';
import AccessTab from "./AccessTab";
import logo from '../../assets/logo.png'


const Header = (props) => {
    const items = props.data.map((item, index) => {
        return (
            <div key={index} className="header__tabs">
                <Link href={item.url} children={props.children}>
                    {item.icon}
                    {item.name}
                </Link>
            </div>
        )
    })
    return (
        <div className="header">
            <img width="100px" height="100px"
                 src={logo} alt="logo"/>
            {items}
            <AccessTab name={props.name} logout={() => props.logout()}/>
        </div>
    )
}

export default Header;
